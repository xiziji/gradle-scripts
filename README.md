# Gradle Scripts

## Common

```gradle
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/repos.gradle'
```

## Deps

```gradle
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/deps/spring-boot-web.gradle'
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/deps/jpa.gradle'
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/deps/redis.gradle'
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/deps/dubbo.gradle'
apply from: 'https://gitlab.com/xiziji/gradle-scripts/-/raw/master/deps/lombok.gradle'
```
